import React, { Component } from 'react';
import FriendsListItem from './friendListItem';
import { routes } from '../../config';
import Banner from "../../shared/assets/crapappbanner.png";


class FriendsListView extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col col-s6 home">
                        <a href={routes.startScreen} className="col col-s6"><img src={Banner} className="homeImage"/></a>
                    </div>
                </div>
                <div className="row">
                    <FriendsListItem />
                    <FriendsListItem />
                    <FriendsListItem />
                </div>
            </div>
        );
    }
}

export default FriendsListView;