import React, { Component } from 'react';
import ErrorPage from '../../shared/components/errorPage';
import PasswordConfirmLoading from "./passwordConfirmLoading";
import {passwordToken} from "../../shared/services/passwordToken";
import PasswordConfirmSucces from "./passwordConfirmSucces";
import PasswordConfirmTokenExpired from "./passwordConfirmTokenExpired";


class PasswordConfirmation extends Component {
    constructor(props) {
        super(props);

        this.state ={
            passwordStatus: 0
        }

        passwordToken().then((response) => {
            this.setState({passwordStatus: response.status});
        });
    }

    render() {
        return (
            this.state.passwordStatus === 0
            ? <PasswordConfirmLoading/>
                : this.state.passwordStatus === 200
            ? <PasswordConfirmSucces/>
                : this.state.passwordStatus === 404
            ? <PasswordConfirmTokenExpired/>
                    : <ErrorPage/>
        )
    }
}

export default PasswordConfirmation;