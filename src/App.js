import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import './App.css';
import LoginView from './login/components/loginView';
import StartScreenView from './startScreen/components/startScreenView';
import { routes } from './config.js';
import UserProfileView from './profile/userProfileView/userProfileView';
import RegisterScreenView from './register/components/registerScreenView';
import SearchFriendsListView from './searchfriendslist/components/searchFriendslistView';
import FriendsListView from './friendslist/components/friendsListView';
import AdminView from './admin/components/adminView';
import PrivateRoute from './shared/components/privateRoute';
import forgotPasswordMail from "./forgotPassword/components/forgotPasswordMail";
import forgotUsernameMail from "./forgotUsername/components/forgotUsernameMail";
import PasswordConfirmation from './newPassword/components/passwordConfirmation';
import UserConfirmationView from './shared/components/userConfirmationView';
import RegisterConfirmationView from './registerConfirmation/components/registerConfirmationView';
import AccountUpdateSuccesView from './shared/components/accountUpdateSuccesView';
import RedirectToBug from './bugs/components/redirectToBug';
import BattleshipSinglePlayer from './BattleshipSinglePlayer/battleshipSinglePlayer';

class App extends Component {

  render() {
    return (
// Perhaps you need <BrowserRouter></BrowserRouter> ? 
    <BrowserRouter>
      <Switch>
        <Route exact path={routes.login} component={LoginView} />
        <Route path={routes.register} component={RegisterScreenView} />
        <Route path={routes.confirmProfileDataUpdate} component={AccountUpdateSuccesView} />
        <Route path={routes.confirmation} component={RegisterConfirmationView} />
        <Route path={routes.confirmationView} component={UserConfirmationView} />
        <Route path={routes.emailForgotPassword} component={forgotPasswordMail} />
        <Route path={routes.emailForgotUsername} component={forgotUsernameMail} />
        <Route path={routes.newPassword} component={PasswordConfirmation} />
        <Route path={routes.bugs} component={RedirectToBug} />
        <PrivateRoute path={routes.startScreen} component={StartScreenView} />
        <PrivateRoute path={routes.userProfile} component={UserProfileView} />
        <PrivateRoute path={routes.searchfriendsList} component={SearchFriendsListView} />
        <PrivateRoute path={routes.friendsList} component={FriendsListView} />
        <PrivateRoute path={routes.singlePlayer} component={BattleshipSinglePlayer} />
        <PrivateRoute path={routes.admin} component={AdminView} />
      </Switch>
    </BrowserRouter>
    );
  }
}

export default App;
