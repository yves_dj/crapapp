import React, { Component, Fragment } from 'react';
import PlayingField from '../shared/components/playingFieldPlayer';
import { getSetup } from '../shared/services/game';
import BattleshipSinglePlayerShipsLoaded from './battleShipSinglePlayerShipsLoaded';
import LoadingScreen from '../shared/components/loadingScreen';

class BattleshipSinglePlayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ships: [],
      gameId: null
    };

    this.getState();
  }

  render() {
    return (
      this.state.ships.length > 0
        ? <BattleshipSinglePlayerShipsLoaded ships={this.state.ships} gameId={this.state.gameId} />
        : <LoadingScreen />
    );
  }

  getState() {
    getSetup().then((setup) => {
      setup.text().then(content => {
        this.setState({ ships: this.getShips(content), gameId: this.getGameId(content) });
      });
    });
  }

  getShips(content) {
    let ships = [[], [], [], [], [], [], [], [], [], []];

    content = JSON.parse(content);
    content.board1.ships.forEach(ship => {
      ship.active.forEach(shipContent => {
        ships[shipContent.row - 1].push(shipContent.column);
      })
    });

    return ships;
  }

  getGameId(content) {
    content = JSON.parse(content);

    return content.id;
  }
}

export default BattleshipSinglePlayer;
